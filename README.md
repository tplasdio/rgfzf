<p align="center">
  <h1 align="center">rgfzf</h1>
  <p align="center">
    Fuzzy find files' contents in your terminal
  </p>
</p>

## 📰 Description

A small script to combine ripgrep (or ripgrep-all)
and fzf to fuzzy find lines of files recursively
under the current directory, and select its file name

## 🚀 Installation

<details>
<summary>📦 Manually</summary>

- *Dependencies*:
  - bash
  - [rg](https://github.com/BurntSushi/ripgrep) or you may just simply GNU grep
  - [fzf](https://github.com/junegunn/fzf)
  - [getoptions](https://github.com/ko1nksm/getoptions)
  - [bat](https://github.com/sharkdp/bat), optional for previews
  - [rga](https://github.com/phiresky/ripgrep-all), optional (and its optional dependencies) for searching files other than plain text ones

```sh
curl -O https://codeberg.org/tplasdio/rgfzf/src/branch/main/rgfzf
sudo mv rgfzf /usr/bin/local/rgfzf
sudo chmod +x /usr/bin/local/rgfzf
```

</details>

<details>
<summary>Arch Linux or other pacman distros</summary>

```sh
curl -O https://codeberg.org/tplasdio/rgfzf/raw/branch/main/packaging/PKGBUILD-git
PACMAN=yay makepkg -sip PKGBUILD-git  # ← or your AUR helper
```

For uninstallation, run `sudo pacman -Rsn rgfzf-git`.

</details>

## ⭐ Usage
```sh
rgfzf           # Fuzzy find lines of files
rgfzf 'search'  # Having first filtered for some search term
rgfzf -p        # With a preview window
rgfzf -d        # Enable filter switching between fzf and rg
rgfzf -e        # Open with $EDITOR after selection
rgfzf --rg=rga  # Using rga to search in files other than plain text
rgfzf --help    # Other options
```

## 👀 See also

- [skim](https://github.com/lotabout/skim#as-interactive-interface) as interactive interface
- [telescope](https://github.com/nvim-telescope/telescope.nvim) for Neovim
- [fzf examples](https://github.com/junegunn/fzf/blob/master/ADVANCED.md#switching-between-ripgrep-mode-and-fzf-mode)

## 📝 License

BSD-3clause
