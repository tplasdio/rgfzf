��          �      ,      �     �     �     �     �     �     �     �  #   �          4     C     P  '   W          �     �  C  �  (   	     2     F  	   [     e     w  	   �  .   �  (   �     �     �                5      D     e                                    	      
                                    Depth of directory recursion Header title Multiple selection OPTIONS: Open with $EDITOR Open with $OPENER Prompt Seconds to wait till reload command Show a preview window Show this help Show version USAGE: Use double filtering between rg and fzf Use rg with fzf [OPTIONS] [SEARCH_TERMS] rg program (rg, rga or grep) Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-08-11 03:52+0000
Last-Translator:  <noreply@example.com>
Language-Team: Spanish <es@tp.org.es>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Profundidad de recursión en directorios Título de cabecera Selección múltiple OPCIONES: Abrir con $EDITOR Abrir con $OPENER Indicador Segundos a esperar para re ejecutar el comando Mostrar una ventana de previsualización Mostrar esta ayuda Mostrar versión USO: Usar doble filtro entre rg y fzf Usa rg con fzf [OPCIONES] [TÉRMINOS_BÚSQUEDA] Programa rg (rg, rga o grep) 